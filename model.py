import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os

a = [1,2,3]
b = [2,4,6]

model = tf.keras.Sequential([
      tf.keras.layers.Dense(208,activation='relu'),
      tf.keras.layers.Dense(100,activation='softmax'),
      tf.keras.layers.Dense(10)


])

model.compile(optimizer='adam',loss='mse')

model.fit(a,b,epochs=1000)
